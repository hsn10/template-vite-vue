export default {
  extends: [
     "stylelint-config-standard-scss", "stylelint-config-standard-vue/scss"
  ],
  rules: {
         'at-rule-no-unknown': null,
    'scss/at-rule-no-unknown': [ true, { 'ignoreAtRules': ['tailwind'] } ],
    'declaration-empty-line-before': [ 'always', { except: [ 'first-nested' ], 'ignore': [ 'after-declaration'] } ],
    'color-hex-length': [ 'long' ]
  }
};
