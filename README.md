# Vite based Vue 3 JS template

This template help you get started developing with Vue 3 in Vite.
The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Included base componenets
* [Vue 3](https://vuejs.org)
* [JSX components](https://vuejs.org/guide/extras/render-function#jsx-tsx)
* [Vite 5](https://vitejs.dev)
* [Vitest 1](https://vitest.dev)

## Linters
* [ESLint 8](https://eslint.org)
* [Style lint 16](https://stylelint.io)

## Formatters
* [Prettier 3](https://prettier.io)

## CSS tools
* [Sass 1](https://sass-lang.com)
* [Autoprefixer 10](https://autoprefixer.github.io)
* [PostCSS 8](https://postcss.org)
* [tailwind 3](https://tailwindcss.com)

## IDE Setup

Recommended IDE - [VS Code](https://code.visualstudio.com/)

### VS Code Plugins
- [Vue - Official](https://marketplace.visualstudio.com/items?itemName=Vue.volar)
- [Vue.js Extension Pack](https://marketplace.visualstudio.com/items?itemName=mubaidr.vuejs-extension-pack)
