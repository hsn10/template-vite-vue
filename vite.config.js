import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import eslintPlugin from 'vite-plugin-eslint'
import legacy from '@vitejs/plugin-legacy'
import tailwindcss from 'tailwindcss';
import autoprefixer from 'autoprefixer';
import stylelint from 'vite-plugin-stylelint';

// https://vitejs.dev/config/
export default defineConfig({
   build: {
      postcss: [
         tailwindcss('./tailwind.config.js'),
         autoprefixer(),
      ],
   },
   plugins: [ vue(), vueJsx(), eslintPlugin(), stylelint(), legacy({
      targets: ['defaults', 'ios>=14', 'firefox esr', '>0.1%' ]
      }),
   ],
})
